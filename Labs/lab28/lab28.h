/** lab28.h
* ===========================================================
* Name: FIRST LAST, DATE
* Section: SECTION
* Project: Programming Assessment 3 Practice
* Documentation: DOC STATEMENT
* ===========================================================  */
#include <stdio.h>
#include <stdlib.h>
#ifndef MYEXE_LAB28_H
#define MYEXE_LAB28_H

#include <string.h>
typedef struct BAHRecord{
    char state[50];
    char city[50];
    double withDep;
    double  withoutDep;
} BAHRecord;
int getNumLines(char filename[]);
int readFile(char filename[], BAHRecord* BAHptr, int records);
double greatestRate(BAHRecord* BAHptr,int count, char* capture);
double leastRate(BAHRecord* BAHptr,int count,char* capture1);
int numStates(BAHRecord* Bahptr,int count);
char* longestCity( BAHRecord* BAHptr,int count,char* capture2);
int highRates(BAHRecord* BAHptr,int count);
void updateState(BAHRecord* BAHptr,int count);
#endif //MYEXE_LAB28_H
