/** lab28.c
* ===========================================================
* Name: FIRST LAST, DATE
* Section: SECTION
* Project: Progamming Assessment 3 Practice
* Documentation: DOC STATEMENT
* ===========================================================  */
#include "lab28.h"
int main(void)
{
    // 1) Define a structure type, named BAHRecord, with appropriate members
    //   to store data from the O1_BAH_2018.txt file. Use the fields names:
    //   state, city, withDep, and withoutDep.

    // 2) Dynamically allocate memory to store the BAH data in the text file.
   int count;
   count=getNumLines("../Labs/lab28/O1_BAH_2018.txt");
    BAHRecord* BAHptr= malloc(sizeof(BAHRecord)*count);

    readFile("../Labs/lab28/O1_BAH_2018.txt",BAHptr,count);
    // 3) Create a user-defined function called readFile, which
    //   requires the filename, memory passed by reference, and
    //   the number of records to be passed as parameters (in that order).
    //   It returns the actual number of records read.
    
    
    // 4) Create a user-defined function called greatestRate to determine
    //   the city, state, and amount of the highest with-dependent rate.
    //   This function accepts the stored data, the number of records, and
    //   a pointer to a char array to capture the city and state (in that order).
    //   It returns the greatest rate.
    char* capture[50];
    greatestRate(BAHptr,count,*capture);
    
    // 5) Create a user-defined function called leastRate to determine
    //   the city, state, and amount of the least without-dependent rate.
    //   This function accepts the stored data, the number of records, and
    //   a pointer to a char array to capture the city and state (in that order).  
    //   It returns the least rate.
    char* capture1[50];
    leastRate(BAHptr,count,*capture1);
    
    // 6) Create a user-defined function called numStates to determine
    //   the number of states represented in the file.  This function accepts the
    //   stored data and the number of records (in that order).  It returns the 
    //   number of states.
    //   Assume that the entries are sorted alphabetically by state.
 numStates(BAHptr,count);

    // 7) Create a user-defined function called highRates to determine
    //   the number of cities with BAH rates (with or without dependent)
    //   above $1,000.  This function accepts the stored data and the number
    //   of records (in that order).  It returns the number of cities.
    highRates(BAHptr,count);
    
    // 8) Create a user-defined function called longestCity to determine
    //   the city with the longest name.  This function accepts the stored data 
    //   the number of records, and a pointer to a character array to capture the city.
    //   It returns a pointer to the character array storing the city name as a string.
    char* capture2[50];
longestCity(BAHptr,count,*capture2);

    // 9) Create a user-defined function called updateState to update the abbreviation
    //   for Colorado from "CO" to "Colorado" for each city/state pair.  Once these
    //   updates are complete, then write all the data to a file called "outputFile.txt".
    //   This function accepts the stored data and the number of records (in that order). 
    //   It does not have a return value.
    updateState(BAHptr,count);
    printf("%s", *capture1);

   return 0;
}
int getNumLines(char filename[]) {
    char c;
    int count = 0;
    // Open the file
    FILE *fp = fopen(filename, "r");

    // Check if file exists
    if (fp == NULL) {
        printf("Could not open file %s", filename);
        return 0;
    }

    // Extract characters from file and store in character c
    for (c = getc(fp); c != EOF; c = getc(fp))
        if (c == '\n') // Increment count if this character is newline
            count = count + 1;

    // Close the file
    fclose(fp);

    return count;

}
int readFile(char filename[], BAHRecord* BAhPtr, int records){

    int i = 0;
    FILE *fp = fopen(filename, "r");
    if (fp == NULL) {
        printf("Bad things\n");}
    while (!feof(fp) && i<records){
        fscanf(fp,"%[^,],%[^,],%lf,%lf%*c",BAhPtr[i].state,BAhPtr[i].city,&BAhPtr[i].withDep,&BAhPtr[i].withoutDep);
        i++;
    }
    fclose(fp);
    return i;
}
double greatestRate(BAHRecord* BAHptr,int count, char* capture){

    int i=0;
    int ans=0;
    for(i=0;i<count;i++){
        if (BAHptr[i].withDep> BAHptr[ans].withDep) {
            ans=i;
        }

    }
    strcpy(capture,BAHptr[ans].city);
    strcat(capture,BAHptr[ans].state);

    return BAHptr[ans].withDep;
}
double leastRate(BAHRecord* BAHptr,int count,char* capture1){

    int i=0;
    int ans=0;
    for(i=0;i<count;i++){
        if (BAHptr[i].withoutDep<BAHptr[ans].withoutDep) {
            ans=i;
        }

    }
    strcpy(capture1,BAHptr[ans].city);
    strcat(capture1,BAHptr[ans].state);

    return BAHptr[ans].withoutDep;


}
int numStates(BAHRecord* Bahptr,int count){

    int i=0;
    int number=0;
    for(i=0;i<count;i++){
            if (strcmp(Bahptr[i].state, Bahptr[i+1].state) !=0) { number=number+1;}
        }

    return number;
}
int highRates(BAHRecord* BAHptr,int count){
    int number=0;
    int i=0;
    for(i=0;i<count;i++){
        if(BAHptr[i].withoutDep>1000 || BAHptr[i].withDep>1000) {

            number= number+1;
        }
    }

    return number;

}
char* longestCity( BAHRecord* BAHptr,int count,char* capture2){

    int i=0;
    int next=0;
    int number=0;
    for(i=0;i<count;i++){
        for(next=0;next<count;next++) {
            if (strlen(BAHptr[i].city)> strlen(BAHptr[number].city)) {
                number=i;
            }
        }

    }
    strcpy(capture2,BAHptr[number].city);

    return capture2;
}
void updateState(BAHRecord* BAHptr,int count){

int i =0;
for(i=0;i<count;i++) {
if (strcmp(BAHptr[i].state,"CO")==0) {
   strcpy(BAHptr[i].state,"Colorado");
}

}
    FILE *fp = fopen("outputFile.txt", "w");
    if (fp == NULL) {
        printf("Bad things\n");}

        fwrite(BAHptr,sizeof(BAHRecord),count,fp);
    fclose(fp);
}