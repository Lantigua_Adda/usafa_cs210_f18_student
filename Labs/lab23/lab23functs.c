//
// Created by C21Adda.Lantigua on 10/17/2018.
//
#include "lab23functs.h"
int getNumRecs(char dataFile[]){
int numrec;
    FILE *filePtr = fopen(dataFile, "r");
    if (filePtr == NULL) {
        printf("Bad things\n");
       numrec=-1;
    }

    fscanf(filePtr,"%d",&numrec);
fclose(filePtr);
    return numrec;
}



void getDataText(CadetInfoStructType cadetRecords[], int numRecs, char dataFile[]){

    FILE *filePtr = fopen(dataFile, "r");
    if (filePtr == NULL) {
        printf("Bad things\n");}

        fseek(filePtr, sizeof(int), SEEK_SET);
    char firstName[30];
    char lastName[45];
    int numRead=0;
    while (numRead < numRecs && !feof(filePtr)) {
        fscanf(filePtr, "%s %s %d %d %d", firstName, lastName, &cadetRecords[numRead].age,
               &cadetRecords[numRead].squad, &cadetRecords[numRead].year);
        strcat(firstName, " ");
        strcpy(cadetRecords[numRead].name, strcat(firstName, lastName));
        numRead++;
    }

fclose(filePtr);
}