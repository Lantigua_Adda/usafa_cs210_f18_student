//
// Created by C21Adda.Lantigua on 10/23/2018.
//
#include <stdlib.h>
#include "lab25.h"

int main() {
    Point first;
    Point secnd;

    getPoints(&first, &secnd);

    Point third = first;

    Point fourth = secnd;

    getDistance(third, fourth);
    printf("%lf", getDistance(third, fourth));

    int number = getNumLines("./lab25Data.csv");
    USAFBaseData **baseStruct = malloc(number * sizeof(USAFBaseData *));
    for (int r = 0; r < number; r++) {
        baseStruct[r] = (malloc(sizeof(USAFBaseData)));


    }


    readFile("./lab25Data.csv", baseStruct, number);

    printData(baseStruct, number);


    return 0;
}

int getNumLines(char filename[]) {
    char c;
    int count = 0;
    // Open the file
    FILE *fp = fopen(filename, "r");

    // Check if file exists
    if (fp == NULL) {
        printf("Could not open file %s", filename);
        return 0;
    }

    // Extract characters from file and store in character c
    for (c = getc(fp); c != EOF; c = getc(fp))
        if (c == '\n') // Increment count if this character is newline
            count = count + 1;

    // Close the file
    fclose(fp);

    return count;
}


void readFile(char filename[], USAFBaseData** baseStruct, int lines){
    char buffer[100];
    int i = 0;
    FILE *fp = fopen(filename, "r");
    fgets(buffer, 100, fp);
    while (!feof(fp) && i<lines){
        fscanf(fp, " %[^,], %d, %d, %[^,], %[^\n]%*c", baseStruct[i]->baseName, &baseStruct[i]->bldgsOwned, &baseStruct[i]->structsOwned, baseStruct[i]->city, baseStruct[i]->state);
        i++;
    }
    fclose(fp);
}


void printData(USAFBaseData** bases, int numBases){
    for (int i = 0; i < numBases; i++) {
        printf("%s %d %d %s %s", bases[i]->baseName, bases[i]->bldgsOwned, bases[i]->structsOwned, bases[i]->city, bases[i]->state);
    }
}



void getPoints(Point *val1, Point *val2) {
    printf("Enter two numbers\n");
    scanf("%d %d", &val1->x, &val1->y);
    scanf("%d %d", &val2->x, &val2->y);

}

double getDistance(Point val3, Point val4) {
    double ans = sqrt(((pow((val4.x - val3.x), 2)) + (pow((val4.y - val3.y), 2))));
    return ans;
}