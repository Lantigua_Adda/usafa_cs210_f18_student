//
// Created by C21Adda.Lantigua on 10/23/2018.
//

#ifndef MYEXE_LAB25_H
#define MYEXE_LAB25_H

#include <math.h>
#include <stdio.h>
typedef struct Point {
    int y;
    int x;
} Point;
typedef struct USAFBaseData {
    char baseName[50];
     int bldgsOwned;
     int structsOwned;
     char city[50];
     char state[30];
}USAFBaseData;
void getPoints(Point* val, Point* val2);
double getDistance(Point val3,Point val4);
int getNumLines(char filename[]);
void readFile(char filename[], USAFBaseData** baseStruct, int lines);
void printData(USAFBaseData** bases, int numBases);
#endif //MYEXE_LAB25_H

//fscanf(fileptr,"%[^,], %d, &d, %[^,],%[^\n]%*c", baseStruct
//%*c == skips any character