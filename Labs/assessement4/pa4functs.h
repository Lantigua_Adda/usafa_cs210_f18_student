/** pa4functs.h
* ===========================================================
* Name: adda Lantigua,
* Section: SECTION
* ===========================================================  */

#ifndef MYEXE_PA4FUNCTS_H
#define MYEXE_PA4FUNCTS_H
int nth_even(int N);
int almost_fibonacci(int N);
#include <stdint.h>
#include <stdio.h>
int alternating_cubes(int N);
int is_positive(int x);
int mult_by_two(int x);
int set_bit(int val, int N, int bitVal);
int quick_hash(char message[]);
#endif //MYEXE_PA4FUNCTS_H
