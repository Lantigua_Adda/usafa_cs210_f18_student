//
// Created by Troy Weingart on 11/4/18.
//

#ifndef MYEXE_BOUNCE_H
#define MYEXE_BOUNCE_H

#include <curses.h>
#include <locale.h>
#include <wchar.h>

#define DELAY 35000

typedef struct {
    int x;
    int y;
    int color;
    int dirX;
    int dirY;
} BallType;


#endif //MYEXE_BOUNCE_H
