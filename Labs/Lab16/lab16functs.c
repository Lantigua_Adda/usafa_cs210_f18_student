//
// Created by C21Adda.Lantigua on 9/25/2018.
//
#include "lab16functs.h"
void captureOhms( double resVals[NUM_RES]) {

    int i=0;
    double numres;
    for (i=0; i<NUM_RES;i++) {
        printf ("Enter Res values\n");

        scanf("%lf",&numres);
        resVals[i]=numres;

    }
}
void calculateCurrent(double circVolt,double resVals[NUM_RES] , double* current) {
    int i=0;
    double Rsum=0;
    for (i=0; i<NUM_RES;i++) {
        Rsum = Rsum + resVals[i];
    }
    *current =circVolt/Rsum;

}
void voltageDrop(double vDrop[NUM_RES],double resVals[NUM_RES],double current){

    int i=0;
    for (i=0; i < NUM_RES; i++) {
        vDrop[i]=current*resVals[i];
    }

}
void printDrop (double vDrop[NUM_RES]) {
    int i=0;
    for (i=0;i<NUM_RES;i++) {
        printf("%d) %.1lf V."
               "\n",i+1,vDrop[i]);

    }
}