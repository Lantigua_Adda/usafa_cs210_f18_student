//
// Created by C21Adda.Lantigua on 9/25/2018.
//

#ifndef MYEXE_LAB16FUNCTS_H
#define MYEXE_LAB16FUNCTS_H
#define NUM_RES 5
#include <stdio.h>
#include <math.h>
void captureOhms( double resVals[NUM_RES]);
void calculateCurrent(double circVolt, double resVals[NUM_RES], double* current);
void voltageDrop(double vDrop[NUM_RES],double resVals[NUM_RES],  double Current );
void printDrop (double vDrop[NUM_RES]);
#endif //MYEXE_LAB16FUNCTS_H
