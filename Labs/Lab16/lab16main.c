//
// Created by C21Adda.Lantigua on 9/25/2018.
//

#include "lab16functs.h"

int main() {
   double resVals[NUM_RES];
    double circVolt = 12;
    double vDrop[NUM_RES];
 //   double currentVal;

 captureOhms(resVals);
 double current=0;
    calculateCurrent(circVolt, resVals, &current);

    voltageDrop(vDrop, resVals,current);
    printDrop(vDrop);
}