#include <stdio.h>

int notComplete()
{
   return 0;
}

int complete()
{
   return 1;
}

int main(void)
{
   int prepComplete = notComplete();
   printf("%d\n", prepComplete);
}