//
// Name: 
//
#include "pa3functs.h"
int main() {
    char letter='c';
    int recs = getNumLines("./dataFile.csv");
    AirCraftType* dataPtr= malloc(recs* sizeof(AirCraftType));
    char* region[60];
    readFile("./dataFile.csv", dataPtr, recs);
    findIndexMostWeight( dataPtr, recs);
    findPtrMostWeight( dataPtr, recs);
    countRegion(dataPtr, recs,*region );
    updateType(dataPtr,  recs);
    writeNewFile( dataPtr,  recs);
    countLetter( dataPtr,recs, letter);
    uniqueManufact( dataPtr, recs);
}