//
// Name:
//
#include <ctype.h>
#include "pa3functs.h"
int getNumLines(char filename[]) {
    char c;
    int count = 0;
    // Open the file
    FILE *fp = fopen(filename, "r");

    // Check if file exists
    if (fp == NULL) {
        printf("Could not open file %s", filename);
        return 0;
    }

    // Extract characters from file and store in character c
    for (c = getc(fp); c != EOF; c = getc(fp))
        if (c == '\n') // Increment count if this character is newline
            count = count + 1;

    // Close the file
    fclose(fp);

    return count;

}
void readFile(char filename[], AirCraftType* dataPtr, int recs){

    int i = 0;
    FILE *fp = fopen(filename, "r");
    if (fp == NULL) {
        printf("Bad things\n");}
    while (!feof(fp) && i<recs){
        fscanf(fp,"%[^,],%[^,],%[^,],%[^,],%[^,],%[^,],%[^,],%d,%d%*c%*c",dataPtr[i].operAirline,dataPtr[i].geoRegion,dataPtr[i].acType,dataPtr[i].acBody,dataPtr[i].acManu,dataPtr[i].acMod,dataPtr[i].acVer,&dataPtr[i].landCount,&dataPtr[i].weight);
    i++;
}
fclose(fp);

}
int findIndexMostWeight(AirCraftType* dataPtr, int recs){

    int i=0;
    int ans=0;
    for(i=0;i<recs;i++){
        if (dataPtr[i].weight>dataPtr[ans].weight) {
            ans=i;
        }

    }

    return ans;


}
AirCraftType* findPtrMostWeight(AirCraftType* dataPtr, int recs){
    int i=0;
    int ans=0;
    for(i=0;i<recs;i++){
        if (dataPtr[i].weight>dataPtr[ans].weight) {
            ans=i;
        }

    }
    return &dataPtr[ans];


}
int countRegion(AirCraftType* dataPtr, int recs, char* region){
    int i=0;
    int count =0;
    for(i=0;i<recs;i++){
        if (strcmp(dataPtr[i].geoRegion,region)==0) {count =(count)+1;}


        }


    return count;
}

void updateType(AirCraftType* dataPtr, int recs){

    int i=0;
    for(i=0;i<recs;i++){
        if (strcmp(dataPtr[i].acType,"Passenger")==0){
        strcpy(dataPtr[i].acType,"PA");}
    }

}
void writeNewFile(AirCraftType* dataPtr, int recs){
    FILE *fp = fopen("outputFile.txt", "w");
    if (fp == NULL) {
        printf("Bad things\n");}

    fwrite(dataPtr,sizeof(AirCraftType),recs,fp);
    fclose(fp);

}
int countLetter(AirCraftType* dataPtr, int recs, char letter){
    int i=0;
    int count=0;

    for (i=0;i<recs;i++){
        if (strchr(dataPtr[i].acManu,letter) != NULL || strchr(dataPtr[i].acManu,letter-32) != NULL ||strchr(dataPtr[i].acManu,letter+32) != NULL){ count=count+1;}

    }


    return count;
}

int uniqueManufact(AirCraftType* dataPtr, int recs){
char uNames[100][50];
    int i=0;
   int j=0;
   int count=0;
    for(i=0;i<recs;i++) {
        for (j = 0; j < recs; j++) {
            if (strcmp(dataPtr[i].acManu, dataPtr[j].acManu) != 0)
                 {strcpy(uName[100][i],dataPtr[i]);count=count+1;
            }
        }
    }
    return count;
}


