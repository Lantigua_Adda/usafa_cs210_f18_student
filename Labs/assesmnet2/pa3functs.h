//
// Name:  
//

#ifndef MYEXE_PA3FUNCTS_H
#define MYEXE_PA3FUNCTS_H
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
typedef struct {
    char operAirline[50];
    char geoRegion[30];
    char acType[30];
    char acBody[30];
    char acManu[30];
    char acMod[30];
    char acVer[30];
    int landCount;
    int weight;
}AirCraftType;
int getNumLines(char filename[]);
void readFile(char filename[], AirCraftType* dataPtr, int recs);
int findIndexMostWeight(AirCraftType* dataPtr, int recs);
AirCraftType* findPtrMostWeight(AirCraftType* dataPtr, int recs);
int countRegion(AirCraftType* dataPtr, int recs, char* region);
void updateType(AirCraftType* dataPtr, int recs);
void writeNewFile(AirCraftType* dataPtr, int recs);
int countLetter(AirCraftType* dataPtr, int recs, char letter);
int uniqueManufact(AirCraftType* dataPtr, int recs);
#endif //MYEXE_PA3FUNCTS_H