//
// Created by C21Adda.Lantigua on 9/19/2018.
//
#include "lab15fbFuncts.h"

#include "lab15functs.h"

int main() {

    int array[MAXPLAYERS][DATACOLS];

    readData(array);

    printf("%d players data read.\n", readData(array));

    printf("The player ID 0 is %s and he scored %d touchdowns in 2017.\n", getPlayerName(0), array[0][2]);

    printf("The player ID 122 is %s and he rushed for %d yards in 2017.\n", getPlayerName(122), array[122][1]);


    int maxnum = 0;
    int max = array[maxnum][maxnum];
    int i = 0;
    int c = 1;
    for (i = 0; i < 316; i++) {

        if (array[i][c] > max) {
            max = array[i][c];
            maxnum = i;
        }
    }


    printf("The player with the most yards in 2017 is %s and he had %d.\n", getPlayerName(maxnum), max);


    int TDsmost = 0;
    int touches = array[TDsmost][c];
    i = 0;
    c = 2;
    for (i = 0; i < 316; i++) {
        if (array[i][c] > array[TDsmost][c]) {
            touches = array[i][c];
            TDsmost = i;
        }

    }
    printf("The player with the most TDs in 2017 is %s and he had %d.\n", getPlayerName(TDsmost), touches);

    int num10 = 0;
    i = 0;
    c = 2;
    for (i = 0; i < 316; i++) {
        if (array[i][c] > 10) {

            num10 = num10 + 1;

        }
    }
    printf("The number of players with more than 10 TDs in 2017 is %d.\n", num10);

    int yards1000 = 0;
    i = 0;
    c = 1;
    for (i = 0; i < 316; i++) {
        if (array[i][c] > 1000) { yards1000 = yards1000 + 1; }
    }
    printf("The number of players with more than 1000 yards in 2017 is %d.\n", yards1000);


    int attemps100 = 0;
    i = 0;
    c = 0;
    for (i = 0; i < 316; i++) {
        if (array[i][c] > 100) { attemps100 = attemps100 + 1; }
    }
    printf("The number of players with more than 100 attempts in 2017 is %d.\n", attemps100);


    int yardsC100 = 0;
    i = 0;
    c = 0;
    for (i = 0; i < 316; i++) {

        if (array[i][c] > 100 && (((double) array[i][1] / (double) array[i][c]) >
                                  ((double) array[yardsC100][1] / (double) array[yardsC100][c]))) { yardsC100 = i; }

    }
    double number = ((double) array[yardsC100][1] / ((double) array[yardsC100][c]));
    printf("The player with the most yards per carry (>100 attempts) is %s and he had %.1lf.\n",
           getPlayerName(yardsC100), number);


    int yardsC50 = 0;
    i = 0;
    c = 0;
    for (i = 0; i < 316; i++) {

        if (array[i][c] > 50 && array[i][c] <= 100 && (((double) array[i][1] / (double) array[i][c]) >
                                                       ((double) array[yardsC50][1] /
                                                        (double) array[yardsC50][c]))) { yardsC50 = i; }

    }
    double number2 = ((double) array[yardsC50][1] / ((double) array[yardsC50][c]));
    printf("The player with the most yards per carry (>50 attempts) is %s and he had %.1lf.\n", getPlayerName(yardsC50),
           number2);

    int yardsC10 = 0;
    i = 0;
    c = 0;
    for (i = 0; i < 316; i++) {

        if (array[i][c] > 10 && array[i][c] <= 50 && (((double) array[i][1] / (double) array[i][c]) >
                                                      ((double) array[yardsC10][1] /
                                                       (double) array[yardsC10][c]))) { yardsC10 = i; }

    }
    double number3 = ((double) array[yardsC10][1] / ((double) array[yardsC10][c]));
    printf("The player with the most yards per carry (>10 attempts) is %s and he had %.1lf.\n", getPlayerName(yardsC10),
           number3);


}











