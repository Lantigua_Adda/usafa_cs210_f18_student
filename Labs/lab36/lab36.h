/** lab36.h
* ===========================================================
* Name: FIRST LAST, DATE
* Section: SECTION
* Documentation: YOUR DOC STATEMENT
* ===========================================================  */

#ifndef MYEXE_LAB36_H
#define MYEXE_LAB36_H
int sum_squares(int N);
int is_even(int x);
int reverse_bytes(unsigned int val);
int is_palindrome(char message[], int len);
#endif //MYEXE_LAB36_H
