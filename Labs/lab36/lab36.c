/** lab36.c
* ===========================================================
* Name: FIRST LAST, DATE
* Section: SECTION
* Documentation: YOUR DOC STATEMENT
* ===========================================================  */

#include "lab36.h"
#include <stdio.h>

int main() {
    // Exercise 1
    printf("\nExercise 1: sum_squares()\n\n");



    // Exercise 2
    printf("\nExercise 2: is_even()\n\n");



    // Exercise 3
    printf("\nExercise 3: power_two()\n\n");



    // Exercise 4
    printf("\nExercise 4: reverse_bytes()\n\n");



    // Exercise 5
    printf("\nExercise 5: is_palindrome()\n\n");


    return 0;
}

int sum_squares(int N) {

    if (N == 1) { return 1; }
    return (N * N) + sum_squares(N - 1);


}

int is_even(int x) {
    if (x & 1) { return 0; }
    else return 1;

}

int power_two(int N) {

    int new;
    new = (1 << N);

    return new;


}

int reverse_bytes(unsigned int val) {
    int rev = 0;
    for (int i = 0; i < 4; i++) {
    rev = (rev << 8) | (val & 0xFF);
    val >>= 8;
}
    return rev;
    //pad with zero logical
    // right shifting a signed  needs an unsigned data type
    //
  //  temp=(val>>24 & 0xFF | val>>8 & 0xFF| val<<24 & 0xFF00000000);
}

int is_palindrome(char message[], int len) {

    if (len <= 1) { return  1; }

    if (message[0] == message[len - 1]) {
        return is_palindrome(message + 1, len - 2);
    }
    else
        return 0;
}


