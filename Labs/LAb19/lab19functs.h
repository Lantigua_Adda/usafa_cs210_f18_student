//
// Created by C21Adda.Lantigua on 10/3/2018.
//

#ifndef MYEXE_LAB19FUNCTS_H
#define MYEXE_LAB19FUNCTS_H

#include <stdio.h>
#include <string.h>
#define MAX_STR_LEN 81
void replMultiChar(char str1[], char searchChar[], char rChar);
void replStr(char str[], char aChar, char bChar);
int findLocations(char str2[], int locations[],char searchChar1);
int countSeqStr(char str3[], char searchStr[]);
#endif //MYEXE_LAB19FUNCTS_H
