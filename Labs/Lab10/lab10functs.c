//
// Created by C21Adda.Lantigua on 9/7/2018.
//
/*** @file lab10functs.c
 *    @author Adda Lantigua first last
 *    @brief deffintions
 */
#include "lab10functs.h"

bool isFirstHalfAlpha(char letter) {
    return (toupper(letter)>77 || toupper(letter)<65) ? false : true;
}
int typeOfChar(char letter2){
 switch(letter2) {
     case 'a':
     case 'e':
     case 'i':
     case 'o':
     case 'u':
     case 'A':
     case 'E':
     case 'I':
     case 'O':
     case 'U':
         return 0;
         break;
     case 'b':
     case 'c':
     case 'd':
     case 'f':
     case 'g':
     case 'h':
     case 'j':
     case 'k':
     case 'l':
     case 'm':
     case 'n':
     case 'p':
     case 'q':
     case 'r':
     case 's':
     case 't':
     case 'v':
     case 'w':
     case 'x':
     case 'y':
     case 'z':
     case 'B':
     case 'C':
     case 'D':
     case 'F':
     case 'G':
     case 'H':
     case 'J':
     case 'K':
     case 'L':
     case 'M':
     case 'N':
     case 'P':
     case 'Q':
     case 'R':
     case 'S':
     case 'T':
     case 'W':
     case 'X':
     case 'Y':
     case 'Z':
         return 1;
         break;
     default:
         return -1;
         break;
    }
}
bool isMagicChar(char letterb) {
    if (letterb=='@')
    {return true;}
    else {return false;}
}