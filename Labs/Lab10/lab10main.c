//
// Created by C21Adda.Lantigua on 9/7/2018.
//
/*** @file lab10main.c
 *    @author adda lantigua
 *    @brief all the calls
 */
#include "lab10functs.h"
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>

int main() {
    char letter;
    char letter2;
    char letterb;

    printf("Enter a single character to pass to isFirstHalfAlpha():\n");
    scanf(" %c", &letter);
    if (isFirstHalfAlpha(letter)==true) {
        printf("A %c is in the first half of the alphabet.\n",letter);
    }
    else { printf("A %c is NOT in the first half of the alphabet.\n",letter); }

    printf("Enter a single character to pass to typeOfChar():\n");
    scanf(" %c",&letter2);
    switch(typeOfChar(letter2)){
        case 0:
            printf("A %c is a vowel.\n",letter2);
            break;
        case 1:
            printf("A %c is a consonant.\n",letter2);
            break;
        case -1:
            printf("A %c is neither a vowel nor a consonant.\n",letter2);
            break;

    }


printf("Enter a single character to pass to isMagicChar():\n");
    scanf(" %c",&letterb);
    if (isMagicChar(letterb)==true) {printf("A %c is the magic character @.\n",letterb);}
    else {printf("A %c is NOT the magic character @.\n",letterb);}





}


