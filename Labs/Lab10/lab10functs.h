//
// Created by C21Adda.Lantigua on 9/7/2018.
//
/*** @file lab10functs.h
 *    @author adda lantigua
 *    @brief define
 */
#include <stdbool.h>
#include <stdio.h>
#include <ctype.h>
#include <stdlib.h>
#ifndef MYEXE_LAB10FUNCTS_H
#define MYEXE_LAB10FUNCTS_H



bool isFirstHalfAlpha(char letter);

int typeOfChar(char letter2);

bool isMagicChar(char letterb);

#define MAGIC_CHAR '@'
#endif //MYEXE_LAB10FUNCTS_H
