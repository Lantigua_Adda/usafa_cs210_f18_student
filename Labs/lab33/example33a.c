//
// Created by John.Cox on 10/30/2018.
//

#include <stdio.h>

int tail_running_sum(int N, int accum);
int tail_fibonacci(int N, int twoBack, int oneBack);
int tail_how_many_bits(int N, int accum);

int main() {

    // Example 1
    printf("\nExample 1: running sum of values from 1 to (1 thru 10)\n\n");

    // running sum of values from 1 to i
    for (int i = 1; i <= 10; ++i) {
        printf("sum of values from 1 to %2d = %2d\n", i, tail_running_sum(i, 0));

    }


    // Example 2
    printf("\nExample 2: fibonacci sequence as tail recursion\n\n");

    // fibonacci sequence as tail recursion
    for (int i = 0; i <= 16; ++i) {
        printf("fib(%2d) = %3d\n", i, tail_fibonacci(i, 0, 1));

    }


    // Example 3
    printf("\nExample 3: count how many bits required to for N items\n\n");

    // count how many bits required to for N items
    for (int i = 2; i <= 17; ++i) {
        printf("%2d items takes %d bits\n", i, tail_how_many_bits(i, 0));

    }

    return 0;
}

int tail_running_sum(int N, int accum) {

    // Base case (accumulator starts at 0)
    if (N == 1) {
        return accum + 1;
    }

    // Tail recursion, nothing other than the recursive call
    return tail_running_sum(N - 1, accum + N);
}

int tail_fibonacci(int N, int twoBack, int oneBack) {

    // First base case, no recursion, return answer
    if (N < 2) {
        return N;

    // Second base case, return the next value in the sequence
    } else if (N == 2) {
        return twoBack + oneBack;

    }

    // Tail recursion, nothing other than the recursive call
    return tail_fibonacci(N - 1, oneBack, twoBack + oneBack);
}

int tail_how_many_bits(int N, int accum) {

    // Base case, return the accumulator (starts at 0)
    if (N == 1) {
        return accum;
    }

    // Tail recursion, nothing other than the recursive call
    // fun fact ((numer + (denom - 1)) / denom) can be used to
    // round up rather than truncating int division
    return tail_how_many_bits((N+1) / 2, accum + 1);
}