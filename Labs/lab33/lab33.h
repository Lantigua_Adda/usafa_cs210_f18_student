/** lab33.h
* ===========================================================
* Name: FIRST LAST, DATE
* Section: SECTION
* Project: Lab 33
* Documentation: DOC STATEMENT
* ===========================================================  */

#ifndef MYEXE_LAB33_H
#define MYEXE_LAB33_H

#include <stdio.h>
#define DIRNAME "../Labs/lab33/password1/password/"

/** ----------------------------------------------------------
 * @fn int factorial(int N)
 * @brief Recursively calculates the factorial of N
 * @param N, the input parameter
 * @return N!, the value of the factorial of N
 * ----------------------------------------------------------
 */
int factorial(int N);
int tail_factorial(int input, int acum);
int collatz(int num, int acumm);
void decode_password(char array[]);
#endif //MYEXE_LAB33_H
