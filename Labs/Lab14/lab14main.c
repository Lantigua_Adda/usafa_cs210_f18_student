//
// Created by C21Adda.Lantigua on 9/19/2018.
//
#include "lab14fbFuncts.h"

#include "lab14functs.h"

int main() {

    int attempts[MAXPLAYERS];
    int yards[MAXPLAYERS];
    int touchdowns[MAXPLAYERS];

    readData(attempts, yards, touchdowns);

    printf("%d players data read.\n", readData(attempts, yards, touchdowns));

    printf("The player ID 0 is %s and he scored %d touchdowns in 2017.\n", getPlayerName(0), touchdowns[0]);

    printf("The player ID 122 is %s and he rushed for %d yards in 2017.\n", getPlayerName(122), yards[122]);


    int maxnum = 0;
    int max = yards[maxnum];
    int i = 0;
    for (i = 0; i < 316; i++) {

        if (yards[i] > max) {
            max = yards[i];
            maxnum = i;
        }

    }

    printf("The player with the most yards in 2017 is %s and he had %d.\n", getPlayerName(maxnum), yards[maxnum]);


    int TDsmost = 0;
    int touches = touchdowns[TDsmost];
    i = 0;
    for (i = 0; i < 316; i++) {
        if (touchdowns[i] > touches) {
            touches = touchdowns[i];
            TDsmost = i;
        }

    }
    printf("The player with the most TDs in 2017 is %s and he had %d.\n", getPlayerName(TDsmost), touchdowns[TDsmost]);

    int num10 = 0;
    i = 0;
    for (i = 0; i < 316; i++) {
        if (touchdowns[i] > 10) {

            num10 = num10 + 1;

        }
    }
    printf("The number of players with more than 10 TDs in 2017 is %d.\n", num10);

    int yards1000 = 0;
    i = 0;
    for (i = 0; i < 316; i++) {
        if (yards[i] > 1000) { yards1000 = yards1000 + 1; }
    }
    printf("The number of players with more than 1000 yards in 2017 is %d.\n", yards1000);


    int attemps100 = 0;
    i = 0;
    for (i = 0; i < 316; i++) {
        if (attempts[i] > 100) { attemps100 = attemps100 + 1; }
    }
    printf("The number of players with more than 100 attempts in 2017 is %d.\n", attemps100);


    int yardsC100 = 0;
    i = 0;
    for (i = 0; i < 316; i++) {

        if (attempts[i] > 100 && (((double) yards[i] / (double) attempts[i]) >
                                  ((double) yards[yardsC100] / (double) attempts[yardsC100]))) { yardsC100 = i; }

    }
    double number = ((double) yards[yardsC100] / ((double) attempts[yardsC100]));
    printf("The player with the most yards per carry (>100 attempts) is %s and he had %.1lf.\n",
           getPlayerName(yardsC100), number);


    int yardsC50 = 0;
    i = 0;
    for (i = 0; i < 316; i++) {

        if (attempts[i] > 50 && attempts[i] <= 100 && (((double) yards[i] / (double) attempts[i]) >
                                                       ((double) yards[yardsC50] /
                                                        (double) attempts[yardsC50]))) { yardsC50 = i; }

    }
    double number2 = ((double) yards[yardsC50] / ((double) attempts[yardsC50]));
    printf("The player with the most yards per carry (>50 attempts) is %s and he had %.1lf.\n", getPlayerName(yardsC50),
           number2);

    int yardsC10 = 0;
    i = 0;
    for (i = 0; i < 316; i++) {

        if (attempts[i] > 10 && attempts[i] <= 50 && (((double) yards[i] / (double) attempts[i]) >
                                                      ((double) yards[yardsC10] /
                                                       (double) attempts[yardsC10]))) { yardsC10 = i; }

    }
    double number3 = ((double) yards[yardsC10] / ((double) attempts[yardsC10]));
    printf("The player with the most yards per carry (>10 attempts) is %s and he had %.1lf.\n", getPlayerName(yardsC10),
           number3);


}











