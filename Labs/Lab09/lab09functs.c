//
// Created by C21Adda.Lantigua on 9/4/2018.
//
#include <math.h>

double volumeCylinder(double radi, double height) {
return(3.14159*(pow(radi,2))*height);
}
double volumeBox(double depth, double width, double heightb){
return (depth*width*heightb);
}
double degToRad(double deg){
return deg*(3.14/180.0);
}