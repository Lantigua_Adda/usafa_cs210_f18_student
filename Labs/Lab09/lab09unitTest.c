//
// Created by C21Adda.Lantigua on 9/5/2018.
//

#include <stdio.h>
#include <assert.h>
#include "lab09functs.h"
int main () {
    printf("Testing started\n ");
    assert(fabs((volumeCylinder(10.0,10.0)-3141.59000)) <=.00001);
    assert(fabs((volumeCylinder(20.0,10.0)-12566.36000)) <=.00001);
    assert(fabs((volumeCylinder(1,10)-31.41590)) <=.00001);
    assert(fabs((volumeBox(10,10,10)-1000.0)) <=.00001);
    assert(fabs((volumeBox(20,30,60)-36000.0)) <=.00001);
    assert(fabs((volumeBox(25,33,4)-3300.0)) <=.00001);
    assert(fabs((degToRad(10.0)-.17444)) <=.00001);
    assert(fabs((degToRad(70.0)-1.22111)) <=.00001);
    assert(fabs((degToRad(90.0)-1.57000)) <=.00001);
printf("Testing complete\n");
return 0;
}