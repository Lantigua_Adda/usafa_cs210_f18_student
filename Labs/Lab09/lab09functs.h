//
// Created by C21Adda.Lantigua on 9/4/2018.
//

#ifndef MYEXE_LAB09FUNCTS_H
#define MYEXE_LAB09FUNCTS_H
#include <string.h>
#include <math.h>
#include <stdio.h>
double volumeCylinder(double radi, double height);
double volumeBox(double depth, double width, double heightb);
double degToRad(double deg);
#endif //MYEXE_LAB09FUNCTS_H

