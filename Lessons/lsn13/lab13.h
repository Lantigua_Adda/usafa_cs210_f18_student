//
// Created by C21Adda.Lantigua on 9/17/2018.
//
#include <stdio.h>
#include <string.h>

#ifndef MYEXE_LAB13_H
#define MYEXE_LAB13_H

#endif //MYEXE_LAB13_H
void swapPassByValue(int a, int b);
void swapPassByReference(int* numa, int* numb);
int pointerSum (int* num1, int* num2);
void findFactorial(int onenum, long int*secnum);