//
// Created by C21Adda.Lantigua on 9/13/2018.
//
/** lab12main.c
* ===========================================================
* Name: Adda Lantigua, 09/13/2018
* Section: T3/T4
* Project: Lab 12
* Purpose: loops
* Documentation:
* ===========================================================
*/
#include "lab12.h"
int main () {
printf("enter a number");
int val1;
scanf("%d",&val1);
if (isArmstrong(val1)==1){printf("%d is an Armstrong number\n.",val1);}
else {printf("%d is not an Armstrong number.\n",val1);}
 printf("Enter two values fro GCD\n");
int val2;
int val3;
scanf("%d",&val2);
scanf("%d",&val3);
printf("GCD=%d\n",findGCD(val2,val3));
printf("Enter two values for range");
int val4;
int val5;
scanf("%d",&val4);
scanf("%d",&val5);
printRange(val4,val5);

}

int isArmstrong(int val1) {
    int total = 0;
    int rem;
    int number;
    number = val1;
    while (number != 0) {
        rem = number % 10;
        total = total + (rem * rem * rem);
        number = number / 10;
    }
    if (total == val1) {return 1;}
    else {
        return 0;
    }
    }

    int findGCD(int val2, int val3) {
        int i = 1;
        int GCD;
        if (val2 > val3) {
            while (i < val2) {
                if (val2 % i == 0 && val3 % i == 0) {
                    GCD = i;
                }
                i++;
            }
            return GCD;
        } else if (val3 > val2) {
            while (i < val3) {
                if (val2 % i == 0 && val3 % i == 0) {
                    GCD = i;
                }
                i++;
            }
            return GCD;
        } else return 1;
    }

    void printRange(int val4, int val5) {
        int i;
        if (val4 < val5) {
            for (i = val4; i <= val5; i++)
                printf("%d\n", i);
        } else { printf("not a range"); }
    }



