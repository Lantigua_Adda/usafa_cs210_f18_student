//
// Created by C21Adda.Lantigua on 9/13/2018.
//
/** lab12main.c
* ===========================================================
* Name: Adda Lantigua, 09/13/2018
* Section: T3/T4
* Project: Lab 12
* Purpose: loops
* Documentation:
* ===========================================================
*/
#include <stdio.h>
#include <string.h>
int isArmstrong(int val1);
int findGCD(int val2,int val3);
void printRange(int val4, int val5);
#ifndef MYEXE_LSN12HEAD_H
#define MYEXE_LSN12HEAD_H

#endif //MYEXE_LSN12HEAD_H
