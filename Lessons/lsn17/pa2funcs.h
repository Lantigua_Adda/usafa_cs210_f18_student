//
// Created by C21Adda.Lantigua on 9/27/2018.
//

#ifndef MYEXE_PA2FUNCS_H
#define MYEXE_PA2FUNCS_H
#define  SIZE 100
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
void reverseDigits(int num1);

int performOperation(char letter, int num2, int num3);
int perfectAI(char chara[50]);
void getMinMax( int numarray[],int num4, int*num5, int* num6);
int checkPerfect( int rang1);
void fill2D(int array2[SIZE][SIZE]);
double avg2D( int array2[SIZE][SIZE]);
#endif //MYEXE_PA2FUNCS_H
