//
// Created by C21Adda.Lantigua on 10/10/2018.
//
#include <stdio.h>

int main() {
char firstword[50];
char secondword[50];
int balance =0;
    FILE *filePtr = fopen("../Lessons/lsn21/bankdata.txt", "r");

    if (filePtr == NULL) {
        printf("Bad things\n");
        return -1;
    }
    while(!feof(filePtr)) {
        fscanf(filePtr, "%s %s %d", firstword, secondword, &balance);
        printf("%s %s has %d balance\n", firstword, secondword, balance);
    }
    fclose(filePtr);
}