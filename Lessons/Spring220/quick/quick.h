/** lab07.h
* ===========================================================
* Name: Troy Weingart and David Caswell
* Section: n/a
* Project: Lab 7 - Quick Sort
* ===========================================================
*/
#ifndef USAFA_CS220_S18_STUDENT_LAB07_H
#define USAFA_CS220_S18_STUDENT_LAB07_H

// constant representing size of input
#define N 8

//function prototypes
/** -------------------------------------------------------------------
 * quickSort() - Perform a quick sort on a portion of an array, from lBound
 * to rBound
 * @param array - the array to sort
 * @param lBound - the starting index of the sublist to sort
 * @param rBound - the ending index of the sublist to sort
 */
void quickSort(int array[], int lBound, int rBound);

#endif //USAFA_CS220_S18_STUDENT_LAB07_H
