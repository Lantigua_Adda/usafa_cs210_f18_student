//
/** pex1header.h
* ===========================================================
* Name: Adda Lantigua, 09/17/2018
* Section: T3/T4
* Project: PEX1
* Purpose: definitions calls and functions
* Documentation: went to E.I and go my loop to increment the total score correctly. also learned how to fix the format with reformat code.
* ===========================================================
*/
// Created by C21Adda.Lantigua on 9/11/2018.
//
#include <time.h>
#include <stdio.h>
#include <stdlib.h>
const int maxPoints=40;
void gameplay(int* score0);
void gameplay1(int* score1);
void drawDie(int pips);
int rollDie();
void turnStatus(int die,int player, int currentTotal,int score0,int score1);
void gameStatus(int player, int score0, int score1);
int totalScore(int score, int turnTotal);
#ifndef MYEXE_PEX1FUNCTS_H
#define MYEXE_PEX1FUNCTS_H

#endif //MYEXE_PEX1FUNCTS_H
