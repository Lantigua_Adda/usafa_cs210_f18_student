/** pex1main.c
* ===========================================================
* Name: Adda Lantigua, 09/17/2018
* Section: T3/T4
* Project: PEX1
* Purpose: definitions calls and functions
* Documentation: went to E.I and go my loop to increment the total score correctly. also learned how to fix the format with reformat code.
* ===========================================================
*/
//
// Created by C21Adda.Lantigua on 9/11/2018.
//
#include "pex1.h"
int main() {
    srand(time(NULL));
    printf("Pig!\n\n");
    printf("Which die would you like to draw?\n");
    int pips;
    scanf("%d", &pips);
    drawDie(pips);
//this function draws the die with the number of pips.

    rollDie();// give a random number 1 threw 6
    printf("Enter the die,player,curre""t total,player 0 score, and player 1 score (separate with spaces)\n");
    int die, player, curTotal, score0, score1;
    scanf("%d %d %d %d %d", &die, &player, &curTotal, &score0, &score1);
    turnStatus(die, player, curTotal, score0, score1);// updates the player about their points


    gameStatus(player, score0, score1);// checks if anyone has won or not
    int turnTotal = 0;
    printf("Beginning the game Pig!\n");
    int turns0 = 0;
    int turns1 = 0;

    score0 = 0;
    score1 = 0;
    do { //calls the function that plays the game and switches between players exits when the while conditions are not met
        gameplay(&score0);
        turns0 = turns0 + 1;
        printf("your score is %d\n", totalScore(score0, turnTotal));
        gameplay1(&score1);
        turns1 = turns1 + 1;
        printf("your score is %d\n", totalScore(score1, turnTotal));
    } while (score0 < 100 && score1 < 100 && turns1 < 40 && turns0 < 40);

    //  states the winner
    if (score0 >= 100) {
        printf("player 0 has Won");
    } else if (score1 >= 100) {
        printf("player 1 has won");
    } else if ((turns0 >= 40 || turns1 >= 40) && score0 > score1) {
        printf("player 0 has won");
    } else { printf("player 1 has won"); }
}

// game play for player 0
void gameplay(int *score0) {
    printf("it is player 0 turn.\n");
    printf("do you want to keep rolling? 1-yes 0-no");
    int keeproll;
    int turnTotal = 0;
    scanf("%d", &keeproll);
    while (keeproll == 1) {
        int numofdie = rollDie();

        drawDie(numofdie);
        if (numofdie == 1) {
            turnTotal = 0;
            printf("you have pigged out\n");
            break;
        } else {
            turnTotal = turnTotal + numofdie;
            printf("Current score for player 0 is %d\n", turnTotal);
            printf("total score is %d\n", *score0);
        }
        printf("do you want to keep rolling? 1-yes 0-no\n");
        scanf("%d", &keeproll);

    }
    if (keeproll == 0) {
        *score0 = *score0 + turnTotal;
    }
    printf("Players total score is %d\n", *score0);
}

// game play for player 1
void gameplay1(int *score1) {
    printf("it is player 1 turn.\n");
    printf("do you want to keep rolling? 1-yes 0-no\n");
    int keeproll;
    int turnTotal = 0;

    scanf("%d", &keeproll);
    while (keeproll == 1) {
        int numofdie = rollDie();

        drawDie(numofdie);
        if (numofdie == 1) {
            turnTotal = 0;
            printf("you have pigged out\n");
            break;
        } else {
            turnTotal = turnTotal + numofdie;
            printf("Current score for player 1 is %d\n", turnTotal);
            printf("total score is %d\n", *score1);
        }
        printf("do you want to keep rolling? 1-yes 0-no");
        scanf("%d", &keeproll);

    }
    if (keeproll == 0) {
        *score1 = *score1 + turnTotal;
    }
    printf("Players total score is %d\n", *score1);
}
// draws the die
void drawDie(int pips) {
    switch (pips) {
        case 1:
            printf(" ------- \n|       | \n|   *   | \n|       | \n ------- \n");
            break;
        case 2:
            printf(" ------- \n"
                   "| *     |\n"
                   "|       |\n"
                   "|     * |\n"
                   " -------\n");
            break;
        case 3:
            printf(" -------  \n| *     | \n|   *   | \n|     * | \n ------- \n");
            break;
        case 4:
            printf(" -------  \n| *   * | \n|       | \n| *   * | \n ------- \n");
            break;
        case 5:
            printf(" -------  \n| *   * | \n|   *   | \n| *   * | \n ------- \n");
            break;
        case 6:
            printf(" ------- \n"
                   "| *   * |\n"
                   "| *   * |\n"
                   "| *   * |\n"
                   " -------\n");

            break;
        default:
            printf("not a correct die\n");
    }
}
// generates a random number 1-6
int rollDie() {
    int numbrand;


    numbrand = rand() % 6 + 1;
    return numbrand;
}
// updates the player on current score and total score
void turnStatus(int die, int player, int curTotal, int score0, int score1) {
    switch (player) {
        case 0 :
            if (die == 1) {
                printf("Player %d pigged out.\n", player);
            } else {
                printf("Current score for player %d = %d.\n", player, curTotal);
                printf("Total score = %d.\n", score0);
            }
            break;
        case 1:
            if (die == 1) {
                printf("Player %d pigged out.\n", player);
            } else {
                printf("Current score for player %d = %d.\n", player, curTotal);
                printf("Total score = %d.\n", score1);
            }
            break;
        default:
            printf("not a player\n");
    }

}
// determines if there is a winner and tells who
void gameStatus(int player, int score0, int score1) {

    printf("Player 0 score: %d\n", score0);
    printf("Player 1 score: %d\n", score1);
    if (score0 >= 100) {
        printf("Player 0 won!\n");
    } else if (score1 >= 100) {
        printf("player 1 won!\n");
    } else {
        printf("It is Player %d turn.\n", player);
    }


}
// calculates the total score during the  game
int totalScore(int score, int turnTotal) {
    int retscore;
    retscore = turnTotal + score;
    return retscore;
}
