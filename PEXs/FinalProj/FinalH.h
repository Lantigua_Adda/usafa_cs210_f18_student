//
// Created by C21Adda.Lantigua on 11/24/2018.
//

#ifndef MYEXE_FINAL2_H
#define MYEXE_FINAL2_H
//struct type player
typedef struct {
    int turns;
    char names[50];
} player;
#include <windows.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
/**   ----------------------------------------------------------  *
 * * @fn < this function plays the game until someone wins>
 * * @param <int nums is the players number char * place is the board pointer to be changed>
 * * @return <returns  1 if ran without any errors>
 * */
int gameplay(int num,char *place);
/**   ----------------------------------------------------------  *
 * * @fn < this function initializes the players names and pieces.>
 * * @param <sthe struct player holds their name and turns>
 * * @return <void>
 * */
void Initializeplayer(player playerinfo[]);
/**   ----------------------------------------------------------  *
 * * @fn < this function checks if anyone has won yet.>
 * * @param <the char array board is sent and it checks each element and looks for patterns of ones or twos>
 * * @return <returns 1 if player one wins or 2 if player 2 wins , zero for none>
 * */
int CheckWinner(char *board);
/**   ----------------------------------------------------------  *
 * * @fn < this function prints the board ono the screen.>
 * * @param <the char array board is sent and it has 32 elements of '0'>
 * * @return <void>
 * */
void printBoard(char *board);
#endif //MYEXE_FINAL2_H
