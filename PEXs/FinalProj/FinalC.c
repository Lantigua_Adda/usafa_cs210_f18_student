//* ==============================================================
//* Name: Adda Lantigua , Fall 2018
//* Section: T3/4
//* Project: Final Project - ConnectFour
//* Description: plays the game Connect Four until someone wins
//* Documentation Statement:  C3C Sidhu showed me how to make colors. he sent me the numbers need to be placed infront of the printf statement to change the colors
////  watched a video on char arrays for one character. the video showed how to asses that one character location and change it while leaving the rest the same.
//// ued that video to draw thr boards and allocate
////a space for them.
//// used the Pigs game to loop the game until someone wins or runs out of turns
//// used stackoverflow to learn how to get an integer from a user  to keep looping
////until the input that was given is an integer between the range wanted
//// C3C Calvin helped with the char pointer of the char array, showed me how to print each element after they have been updated
////
//* ==============================================================
//* UserManual/Instructions:
//*   best if ran in Cygwin . follow instructions in game
// *
// *  Required Programming Skills (must have 4 of 5):
// *   1) Dynamic Memory Allocation
// *          What you did to meet this req: i made a struct and DMA for two players
// *              File: (FinalC.c player struct in main)
// *              Line#: (77)
// *   2) Pointers
// *          What you did to meet this req: i made a char array and sent it to a function to change it elements
// *              File: FinalC.c
// *              Line#: 143 or 131
// *
// *   4) Structs
// *          What you did to meet this req: made a struct called Player and had an int and a char array inside it
// *              File: FinalH.h
// *              Line#:8
// *
// *   5) String Manipulation
// *          What you did to meet this req: strcpy a players name into another string
// *              File: FinalC.c
// *              Line#:81
// *
// *  Required Advanced Programming Skills (1 or more):
// *   1) Recursion
// *          What you did to meet this req: re called the function gameplay until the right number was given
// *              File: (FinalC.c
// *              Line#: 239
// *
// *   2) 2D Dynamic Memory
// *          What you did to meet this req: N/A
// *              File: N/A
// *              Line#:
// *
// *   3) Graphics - Curses or Win32
// *          What you did to meet this req: N/A
// *              File: N/A
// *              Line#:
// *
// *   List any changes or omissions from stated shall requirements (from
// *   your design) also describe any functionality that isn't working.
// *      Requirement changes/omissions:
// *          1) did not use curses to build game
// *          2)does not play against computer
// *          3)
// *
// *      Errors:
// *          1)
// *          2)
// *
// *   How many hours did you spend on this project: 20-25
// *   How many lines of code did you write in total: 100-200, lots of them are repeated
// *
//*/
// Created by C21Adda.Lantigua on 11/20/2018.


#include "FinalH.h"

int main() {

    player *players = malloc(2 * sizeof(player)); // dynamically allocate memory for players
    printf("\n\n\033[22;32m  *********Welcome to connect four*********\n\n");
    Initializeplayer(players); // also acts like a pointer
    char player1name[50];
    strcpy(player1name, players[0].names); // string manipulation
    char player2name[50];
    strcpy(player2name, players[1].names); // string manipulation
    printf("\033[22;31m %s your piece is '1' \n\n", player1name);
    printf("\033[22;34m %s your piece is '2' \n\n", player2name);
    char board[42];
    // this sets every element inside board as a '0' so it can later be changed to the players number
    for (int i = 0; i < 42; i++) {
        board[i] = '0';
    }
    printBoard(board);
    players[0].turns = 0;
    players[1].turns = 0;
    do {
        printf("\033[22;31mit is %s's  turn\n", player1name);
        gameplay(1, board);
        players[0].turns++;
        printBoard(board);
        if (CheckWinner(board) == 1) {
            printf("\033[22;31m %s has won !\n", player1name);
            break;
        }
        if (CheckWinner(board) == 2) {
            printf("\033[22;34m %s has won !\n", player2name);
            break;
        }
        printf("\033[22;34mit is %s's  turn\n", player2name);
        gameplay(2, board);
        players[1].turns++;
        printBoard(board);
        if (CheckWinner(board) == 1) {
            printf("\033[22;31m %s has won !\n", player1name);
            break;
        }
        if (CheckWinner(board) == 2) {
            printf("\033[22;34 %s has won !\n", player2name);
            break;
        }
    } while (players[0].turns <= 21 || players[1].turns <= 21 || CheckWinner(board) == 0);
    if (players[0].turns > 21 || players[1].turns > 21) {
        printf("no winners\n");
    }

}

/**   ----------------------------------------------------------  *
 * * @fn <  ask for players names and stores them in the pointer of playerinfo.names>
 * * @param <the struct player as a pointer>
 * * @return <void>
 * */
void Initializeplayer(player playerinfo[]) {
    printf("\033[01;31m Enter first players name\n\n");
    scanf("%s", playerinfo[0].names);
    printf("\033[01;34m Enter second players name\n\n");
    scanf("%s", playerinfo[1].names);
}

/**   ----------------------------------------------------------  *
 * * @fn <  prints the board with the elements inside them.>
 * * @param <the pointer of board as a char array>
 * * @return <void>
 * */
void printBoard(char *board) {
    printf("\033[0m\033[22;37m   1     2     3     4     5     6     7\n\n");
    int i = 0;
    int j = 0;
    for (i = 0; i < 6; i++) {
        for (j = 0; j < 7; j++) {
            printf("\033[01;37m [ %c ]", board[7 * i + j]); // %c will later be filled with the players number
        }
        printf("\033[01;36m\n~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n");

    }


}

/**   ----------------------------------------------------------  *
 * * @fn < ask for a number between 1-7 and plays the game depending on the number entered.>
 * * @param <the num is the players piece number 1 or 2 and the char array board is a pointer that will be changed
 * * @return <returns  1 if the game runs smoothly>
 * */
int gameplay(int num, char *board) {
    printf("Enter a move \n");
    char turn;
    if (num == 1) { turn = '1'; }
    else { turn = '2'; }
    char *p;
    char number[100];
    int place = 0; // place is the number on the board that they want to move into
    // only accepts integer from user. used stackoveflow to understand this method
    while (fgets(number, sizeof(number), stdin)) {
        place = strtol(number, &p, 10);
        if (p == number || *p != '\n') {
            printf("Not an integer between(1-7) try again: \n\n");
        } else break;
    }
    printf("You entered: %d\n\n", place);
    int i = 0; // this i is used for the rows. it goes down one every time one row is filled in that spot
    switch (place) {
        case 1:
            for (i = 5; i >= 0; i--) {
                if (board[7 * i + place - 1] == '0') {
                    board[7 * i + place - 1] = turn;
                    break;
                }
            }
            break;
        case 2:
            for (i = 5; i >= 0; i--) {
                if (board[7 * i + place - 1] == '0') {
                    board[7 * i + place - 1] = turn;
                    break;
                }
            }
            break;
        case 3:
            for (i = 5; i >= 0; i--) {
                if (board[7 * i + place - 1] == '0') {
                    board[7 * i + place - 1] = turn;
                    break;
                }
            }
            break;
        case 4:
            for (i = 5; i >= 0; i--) {
                if (board[7 * i + place - 1] == '0') {
                    board[7 * i + place - 1] = turn;
                    break;
                }
            }
            break;
        case 5:
            for (i = 5; i >= 0; i--) {
                if (board[7 * i + place - 1] == '0') {
                    board[7 * i + place - 1] = turn;
                    break;
                }
            }
            break;
        case 6:
            for (i = 5; i >= 0; i--) {
                if (board[7 * i + place - 1] == '0') {
                    board[7 * i + place - 1] = turn;
                    break;
                }
            }
            break;
        case 7:
            for (i = 5; i >= 0; i--) {
                if (board[7 * i + place - 1] == '0') {
                    board[7 * i + place - 1] = turn;
                    break;
                }
            }
            break;
        default:
            printf("not a correct move.\n please try again\n");
            gameplay(num, board);
            break;

    }
    return 1;
}

/**   ----------------------------------------------------------  *
 * * @fn <  checks who has one / who has a pattern  or their piece 4 times in a row.>
 * * @param <the char array board has all the elements changed depending on where they placed their pieces>
 * * @return <returns 1 of player 1 has four in a row , two if player 2 has four in a row and zero otherwise>
 * */
int CheckWinner(char *board) {
    int ans = 0;
    for (int i = 0; i < 6; i++) {
        for (int col = 0; col < 4; col++) {
            if (
                    board[7 * i + col] == board[7 * i + col + 1] &&
                    board[7 * i + col + 1] == board[7 * i + col + 1 * 2] &&
                    board[7 * i + col + 1 * 2] == board[7 * i + col + 1 * 3] &&
                    board[7 * i + col] == '1') {
                ans = 1;
                break;
            }
        }
    }
    for (int i = 0; i < 6; i++) {
        for (int col = 0; col < 4; col++) {
            if (
                    board[7 * i + col] == board[7 * i + col + 1] &&
                    board[7 * i + col + 1] == board[7 * i + col + 1 * 2] &&
                    board[7 * i + col + 1 * 2] == board[7 * i + col + 1 * 3] &&
                    board[7 * i + col] == '2') {
                ans = 2;
                break;
            }
        }
    }

    for (int i = 0; i < 3; i++) {
        for (int col = 0; col < 7; col++) {
            if (
                    board[7 * i + col] == board[7 * i + col + 7] &&
                    board[7 * i + col + 7] == board[7 * i + col + 7 * 2] &&
                    board[7 * i + col + 7 * 2] == board[7 * i + col + 7 * 3] &&
                    board[7 * i + col] == '1') {
                ans = 1;
                break;
            }
        }
    }
    for (int i = 0; i < 3; i++) {
        for (int col = 0; col < 7; col++) {
            if (
                    board[7 * i + col] == board[7 * i + col + 7] &&
                    board[7 * i + col + 7] == board[7 * i + col + 7 * 2] &&
                    board[7 * i + col + 7 * 2] == board[7 * i + col + 7 * 3] &&
                    board[7 * i + col] == '2') {
                ans = 2;
                break;
            }
        }
    }
    int j = 0;
    for (int i = 0; i < 3; i++) {
        for (int col = 0; col < 7; col++) {
            if ((j <= 3 &&
                 board[7 * i + col] == board[7 * i + col + 8] &&
                 board[7 * i + col + 8] == board[7 * i + col + 8 * 2] &&
                 board[7 * i + col + 8 * 2] == board[7 * i + col + 8 * 3] &&
                 board[7 * i + col] == '1') ||
                (j <= 3 &&
                 board[7 * i + col] == board[7 * i + col + 6] &&
                 board[7 * i + col + 6] ==
                 board[7 * i + col + 6 * 2] &&
                 board[7 * i + col + 6 * 2] ==
                 board[7 * i + col + 6 * 3] &&
                 board[7 * i + col] == '1')) {
                ans = 1;
                break;
            }
            j++;
        }
        j = 0;
    }
    j = 0;
    for (int i = 0; i < 3; i++) {
        for (int col = 0; col < 7; col++) {
            if ((j <= 3 &&
                 board[7 * i + col] == board[7 * i + col + 8] &&
                 board[7 * i + col + 8] == board[7 * i + col + 8 * 2] &&
                 board[7 * i + col + 8 * 2] == board[7 * i + col + 8 * 3] &&
                 board[7 * i + col] == '2') ||
                (j <= 3 &&
                 board[7 * i + col] == board[7 * i + col + 6] &&
                 board[7 * i + col + 6] ==
                 board[7 * i + col + 6 * 2] &&
                 board[7 * i + col + 6 * 2] ==
                 board[7 * i + col + 6 * 3] &&
                 board[7 * i + col] == '2')) {
                ans = 2;
                break;
            }
            j++;
        }
        j = 0;
    }
    return ans;
}

