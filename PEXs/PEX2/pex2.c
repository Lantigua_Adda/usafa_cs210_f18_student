//
// Created by C21Adda.Lantigua on 10/5/2018.
//
//
/** pex2header.c
* ===========================================================
* Name: Adda Lantigua, 10/09/2018
* Section: T3/T4
* Project: PEX2
* Purpose: functions and calls in main
* Documentation: watched videos on character arrays and pointers.
* ===========================================================
*/
#include "pex2.h"

int main() {
    printf("hamming = %d\n", hammingDistance("AAA", "CC")); // result= -1
    printf("hamming = %d\n", hammingDistance("ACCT", "ACCG")); //result= 1
    printf("hamming = %d\n", hammingDistance("ACGGT", "CCGTT")); //result= 2
    printf("simularity = %0.3f\n", similarityScore("CCC", "CCC")); // result= 1.0
    printf("simularity = %0.3f\n", similarityScore("ACCT", "ACCG")); // result= 0.75
    //rintf("simularity = %0.3f\n", similarityScore("ACGGT", "CCGTT")); // result= 0.6
    //rintf("simularity = %0.3f\n", similarityScore("CCGCCGCCGA", "CCTCCTCCTA")); // result= 0.7
    //rintf("# matches = %d\n", countMatches("CCGCCGCCGA", "TTT", 0.6)); // result= 0
    printf("# matches = %d\n", countMatches("CCGCCGCCGA", "CCG", 0.2)); // result= 8
    //printf("# matches = %d\n", countMatches(MOUSEDNA, "CGCTT", 0.5)); // result= 36
    //printf("# matches = %d\n", countMatches(HUMANDNA, "CGC", 0.3)); // result= 215
    //printf("best match = %0.3f\n", findBestMatch("CCGCCGCCGA", "TTT")); // result= 0.0
    //printf("best match = %0.3f\n", findBestMatch("CTGCCACCAA", "CCGC")); // result= 0.75
    //rintf("best match = %0.3f\n", findBestMatch(MOUSEDNA, "CGCTT")); // result= 0.8
    //printf("best match = %0.3f\n", findBestMatch(HUMANDNA, "CGC")); // result= 1.0
    printf("likely genome match = %d.\n", findBestGenome(CATDNA, HUMANDNA, MOUSEDNA, "CTTTAGGCCGGTT")); //result= 1
    printf("likely genome match = %d.\n", findBestGenome(CATDNA, HUMANDNA, MOUSEDNA, "CTTTAGGCCGGG")); //result= 2
    printf("likely genome match = %d.\n", findBestGenome(CATDNA, HUMANDNA, MOUSEDNA, "CTTAATTCTTTT")); //result= 3
    printf("likely genome match = %d.\n", findBestGenome(CATDNA, HUMANDNA, MOUSEDNA, "CTTTAG")); //result= 0

}

/**   ----------------------------------------------------------  *
 * * @fn < this function measure the similarity between two strings of the same length.>
 * * @param <str1 and str2 are used to compare each other in determining distance>
 * * @return <returns the number of mismatches in the string>
 * */
int hammingDistance(char *str1, char *str2) {

    int mismatch = 0; // counts the differences
    int i = 0;
    for (i = 0; i < strlen(str1); i++) {
        if (strlen(str1) != strlen(str2)) {
            mismatch = -1;
            break;
        } else if (str1[i] != str2[i]) { mismatch = mismatch + 1; }


    }

    return mismatch;

}
/**   ----------------------------------------------------------  *
 * * @fn < similarity score is determined by getting the size of the string subtracting it from the hamming distance than dividing it by the size>
 * * @param <seq1 and seq2 are compared in the hamming distance than are used in the formula to get similarity score>
 * * @return <returns the similarity score computed>
 * */
float similarityScore(char *seq1, char *seq2) {

    float size = strlen(seq1);
    float distance = hammingDistance(seq1, seq2);
    float similar = 0.0;
    similar = (size - distance) / size;
    return similar;


}
/**   ----------------------------------------------------------  *
 * * @fn < this function counts the number of matches a given sequence has in a given genome.>
 * * @param <genome is a given in which seq is compared to. the minscore
 *  is given and if seq and genome first few characters score is greater than minscore. the count is incremented.>
 * * @return <returns the count of similarity greater than minScore>
 * */

int countMatches(char *genome, char *seq, float minScore) {

    int size = strlen(seq);
    char gene[size + 1];
    char *ptr = genome;
    strncpy(gene, ptr, size);
    gene[size] = '\0';

    int count = 0;
    do {

        if (similarityScore(gene, seq) >= minScore) {
            count = count + 1;
        }
        ptr = (ptr + 1);
        strncpy(gene, ptr, size);
    } while (gene[size - 1] != '\0');
    return count;
}
/**   ----------------------------------------------------------  *
 * * @fn < selects the best match by comparing similarity scores at every position
 *   in the genome for a given sequence and returns the highest one found..>
 * * @param <gemome and seq are given  to compare and get the best match.>
 * * @return <returns the highest match>
 * */
float findBestMatch(char *genome, char *seq) {
    int size = strlen(seq);
    char gene[size + 1];
    char *ptr = genome;
    strncpy(gene, ptr, size);
    gene[size] = '\0';

    float count = 0.0;  // used to store the highest number
    do {

        if (similarityScore(gene, seq) > count) {
            count = similarityScore(gene, seq);
        }
        ptr = (ptr + 1);
        strncpy(gene, ptr, size);
    } while (gene[size - 1] != '\0');


    return count;

}
/**   ----------------------------------------------------------  *
 * * @fn <  takes 3 known genomes  and a sequence to determine which genome is the best match.>
 * * @param <gets three genomes and compares which one has the highest match>
 * * @return <returns the 1, 2,3 or 0 depending on which genome had the highest match>
 * */
int findBestGenome(char *genome1, char *genome2, char *genome3, char *unknownSeq) {
    int num = 0;
    if ((findBestMatch(genome1, unknownSeq) > findBestMatch(genome2, unknownSeq)) &&
        (findBestMatch(genome1, unknownSeq) > findBestMatch(genome3, unknownSeq))) { num = 1; }
    else if ((findBestMatch(genome2, unknownSeq) > findBestMatch(genome1, unknownSeq)) &&
             (findBestMatch(genome2, unknownSeq) > findBestMatch(genome3, unknownSeq))) { num = 2; }
    else if ((findBestMatch(genome3, unknownSeq) > findBestMatch(genome1, unknownSeq)) &&
             (findBestMatch(genome3, unknownSeq) > findBestMatch(genome2, unknownSeq))) { num = 3; }
    else if ((findBestMatch(genome1, unknownSeq) == findBestMatch(genome2, unknownSeq)) ||
             (findBestMatch(genome1, unknownSeq) == findBestMatch(genome3, unknownSeq)) ||
             (findBestMatch(genome2, unknownSeq) == findBestMatch(genome3, unknownSeq))) {
        num = 0;
    }
    return num;
}