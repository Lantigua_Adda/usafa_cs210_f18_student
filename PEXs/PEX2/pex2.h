//
// Created by C21Adda.Lantigua on 10/5/2018.
//
/** pex2header.h
* ===========================================================
* Name: Adda Lantigua, 10/09/2018
* Section: T3/T4
* Project: PEX2
* Purpose: functions prototype and declaration.
* Documentation: watched videos on character arrays and pointers.
* ===========================================================
*/

#ifndef MYEXE_PEX2_H
#define MYEXE_PEX2_H
#include <stdio.h>
#include <string.h>
#define HUMANDNA "CGCAAATTTGCCGGATTTCCTTTGCTGTTCCTGCATGTAGTTTAAACGAGATTGCCAGCACCGGGTATCATTCACCATTTTTCTTTTCGTTAACTTGCCGTCAGCCTTTTCTTTGACCTCTTCTTTCTGTTCATGTGTATTTGCTGTCTCTTAGCCCAGACTTCCCGTGTCCTTTCCACCGGGCCTTTGAGAGGTCACAGGGTCTTGATGCTGTGGTCTTCATCTGCAGGTGTCTGACTTCCAGCAACTGCTGGCCTGTGCCAGGGTGCAGCTGAGCACTGGAGTGGAGTTTTCCTGTGGAGAGGAGCCATGCCTAGAGTGGGATGGGCCATTGTTCATG"
#define MOUSEDNA "CGCAATTTTTACTTAATTCTTTTTCTTTTAATTCATATATTTTTAATATGTTTACTATTAATGGTTATCATTCACCATTTAACTATTTGTTATTTTGACGTCATTTTTTTCTATTTCCTCTTTTTTCAATTCATGTTTATTTTCTGTATTTTTGTTAAGTTTTCACAAGTCTAATATAATTGTCCTTTGAGAGGTTATTTGGTCTATATTTTTTTTTCTTCATCTGTATTTTTATGATTTCATTTAATTGATTTTCATTGACAGGGTTCTGCTGTGTTCTGGATTGTATTTTTCTTGTGGAGAGGAACTATTTCTTGAGTGGGATGTACCTTTGTTCTTG"
#define CATDNA "CGCATTTTTGCCGGTTTTCCTTTGCTGTTTATTCATTTATTTTAAACGATATTTATATCATCGGGTTTCATTCACTATTTTTCTTTTCGATAAATTTTTGTCAGCATTTTCTTTTACCTCTTCTTTCTGTTTATGTTAATTTTCTGTTTCTTAACCCAGTCTTCTCGATTCTTATCTACCGGACCTATTATAGGTCACAGGGTCTTGATGCTTTGGTTTTCATCTGCAAGAGTCTGACTTCCTGCTAATGCTGTTCTGTGTCAGGGTGCATCTGAGCACTGATGTGGAGTTTTCTTGTGGATATGAGCCATTCATAGTGTGGGATGTGCCATAGTTCATG"
/**   ----------------------------------------------------------  *
 * * @fn < this function measure the similarity between two strings of the same length.>
 * * @param <str1 and str2 are used to compare each other in determining distance>
 * * @return <returns the number of mismatches in the string>
 * */
int hammingDistance(char* str1, char* str2);
/**   ----------------------------------------------------------  *
 * * @fn < similarity score is determined by getting the size of the string subtracting it from the hamming distance than dividing it by the size>
 * * @param <seq1 and seq2 are compared in the hamming distance than are used in the formula to get similarity score>
 * * @return <returns the similarity score computed>
 * */
float similarityScore(char* seq1, char* seq2);
/**   ----------------------------------------------------------  *
 * * @fn < this function counts the number of matches a given sequence has in a given genome.>
 * * @param <genome is a given in which seq is compared to. the minscore
 *  is given and if seq and genome first few characters score is greater than minscore. the count is incremented.>
 * * @return <returns the count of similarity greater than minScore>
 * */

int countMatches(char* genome, char* seq, float minScore);
/**   ----------------------------------------------------------  *
 * * @fn < selects the best match by comparing similarity scores at every position
 *   in the genome for a given sequence and returns the highest one found..>
 * * @param <gemome and seq are given  to compare and get the best match.>
 * * @return <returns the highest match>
 * */
float findBestMatch(char* genome, char* seq);
/**   ----------------------------------------------------------  *
 * * @fn <  takes 3 known genomes  and a sequence to determine which genome is the best match.>
 * * @param <gets three genomes and compares which one has the highest match>
 * * @return <returns the 1, 2,3 or 0 depending on which genome had the highest match>
 * */
int findBestGenome(char* genome1, char* genome2, char* genome3, char* unknownSeq);
#endif //MYEXE_PEX2_H
